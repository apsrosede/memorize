//
//  ContentView.swift
//  Memorize
//
//  Created by AdrianRoseMacBookAir on 25.05.20.
//  Copyright © 2020 apsrose. All rights reserved.
//

import SwiftUI

struct EmojiMemoryGameView: View {
    
    @ObservedObject var viewModel: EmojiMemoryGame
    
    var body: some View {
        VStack {
            
        Grid(items: viewModel.cards) { card in
            CardView(card: card).onTapGesture {
                withAnimation(.linear(duration: 0.75)) {
                    self.viewModel.choose(card: card)
                }
            }
            .padding(5)
        }
            .padding()
            .foregroundColor(.orange)
            Button(action: {
                withAnimation(.easeInOut(duration: 0.75)) {
                    self.viewModel.resetGame()
                }
            }, label: { Text("New Game") })
        }
    }
}

struct CardView : View {
    
    var card : MemoryGame<String>.Card
    
    var body: some View {
        GeometryReader { geometry in
            self.body(for: geometry.size)
        }
    }
    
    @State private var animatedBonusRemaining: Double = 0
    
    private func startBonusTimeAnimation () {
        animatedBonusRemaining = card.bonusRemaining
        withAnimation(.linear(duration: card.bonusTimeRemaining)) {
            animatedBonusRemaining = 0
        }
    }
    
    @ViewBuilder
    private func body(for size: CGSize) -> some View {
        
        if card.isFaceUp || !card.isMatched {
             ZStack {
                Group {
                if card.isConsumingBonusTime {
                    Pie(startAngle: Angle.degrees(0-90), endAngle: Angle.degrees(-animatedBonusRemaining*360-90), clockWise: true)
                     .padding(5).opacity(0.4)
                        .onAppear {
                            self.startBonusTimeAnimation()
                        }
                } else {
                    Pie(startAngle: Angle.degrees(0-90), endAngle: Angle.degrees(-animatedBonusRemaining*360-90), clockWise: true)
                 }
                }
                .padding(5).opacity(0.4)
                 Text(self.card.content)
                    .font(Font.system(size: fontSize(for: size)))
                    .rotationEffect(Angle.degrees(card.isMatched ? 360 : 0))
                    .animation(card.isMatched ? Animation.linear(duration: 1.0).repeatForever(autoreverses: false) : .default)
             }
             .cardify(isFaceUp: card.isFaceUp)
             .transition(AnyTransition.scale)
             //.rotation3DEffect(Angle.degrees(card.isFaceUp ? 0 : 180), axis: (0,1,0))
           }
        }
}


    // MARK: - Drawing Constants

private let cornerradius: CGFloat = 10.0
private let edgeLineWidth: CGFloat = 3
private let fontScaleFactor: CGFloat = 0.70

private func fontSize(for size: CGSize) -> CGFloat {
    min(size.width, size.height) * fontScaleFactor
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let game = EmojiMemoryGame()
        game.choose(card: game.cards[6])
        return EmojiMemoryGameView(viewModel: game)
    }
}
