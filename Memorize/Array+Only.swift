//
//  Array+Only.swift
//  Memorize
//
//  Created by AdrianRoseMacBookAir on 19.07.20.
//  Copyright © 2020 apsrose. All rights reserved.
//

import Foundation

extension Array {
    var only: Element? {
        count == 1 ? first : nil
    }
}
